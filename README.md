# PyRetroplay

This is a script that manages updates form [Retroplay's Amiga WHDLoad directories](http://eab.abime.net/showthread.php?t=43633) on [EAB FTP server](http://eab.abime.net/showthread.php?t=61028).

It is written in Python 2 (with ongoing work to make it compatible with Python 3), and was tested on Ubuntu 18.04 64bit on ext4 filesystem (probably doesn't work on NTFS when Amiga files have "funny" names).
It may work on non-Linux systems, but that wasn't tested. Amiga side was tested on real Amiga 1200 with WB 3.1.

## Basic usage

For basic usage, you'll need directory structures as defined by [Turran from EAB](http://eab.abime.net/showthread.php?t=68704):

```
Demos_WHDLoad/
	0-9/
	A/
	B/
	...
Games_WHDLoad/
	0-9/
	A/
	B/
	...
Games_WHDLoad_AGA/
	0-9/
	A/
	B/
	...
```
and Retroplay's directory structures from EAB FTP:

```
Retroplay/
	Commodore_Amiga_-_HD_Loaders_-_Games/
	Commodore_Amiga_-_JST_-_Games/
	Commodore_Amiga_-_WHDLoad_-_Demos/
	Commodore_Amiga_-_WHDLoad_-_Games/
	Commodore_Amiga_-_WHDLoad_-_Games_(Beta_&_Unreleased)/
	Commodore_Amiga_-_WHDLoad_-_Magazines/
```
To get these directories, unpack Directories.tar.gz to the same place where PyRetroplay script is.

It is advisable that you download initial Retroplay's WHDLoad collection from his cloud storage, and to use script for subsequent updates from FTP.
It is also advisable to turn off AmigDOS script generation on first run (set GenerateAmigaScript to False inside PyRetroplay.py).

When script starts, it will check FTP server for new files, download them, and unpack to appropriate directories, updating existing files:

* If archive for new game/demo is found, it will be unpacked
* If new version of existing archive is found, old unpacked version will be deleted, and new one will be unpacked

Script keeps all its data inside local database (actually, just a json file, archivedata.json).
That file just caches information about archives, and makes script execution fast (except for the first run, when it scans all the archives).
If you want to rebuild everything, just delete json file and unpacked games/demos and run the script.

## Update of collection on real Amiga

During unpacking, an AmigaDOS script will be generated, along with directory with all new archives.
That script (and its directory) is intended to be transferred and then run on real Amiga, and it will update existing collection on Amiga HD.
It is assumed that the user has identical directory structures on PC and on Amiga and that script is run on Amiga inside drive where Demos_WHDLoad, Games_WHDLoad and Games_WHDLoad_AGA directories are:
```
Demos_WHDLoad/
Games_WHDLoad/
Games_WHDLoad_AGA/
update2017-04-18-1-files/   <-- directory with new archives
update2017-04-18-1          <-- update script (execute update2017-04-18-1)

```
AmigaDOS update script will check if it is in the right place, ask the user if it wants to update and then repeat the process that PyRetroplay did on PC side:

* If directory inside archive is for new game/demo, it will be unpacked
* If directory inside archive is for existing game/demo, old version will be deleted, and new one will be unpacked

After update is successfully completed, you can delete update script and its directory (or keep it for date reference).

## Requirements

### PC Side
Script now uses 7zip, The Unarchiver [(command-line version)](https://theunarchiver.com/command-line) and lhasa for handling the archives as it turned out that there are archives that can be handled by jost one of them.
On Debian-based systems, you can install them with:
```
sudo apt-get install p7zip-full unar lhasa
```
Other than that, it needs Python 2.7.x or later version of Python 2 and progressbar library (python-progressbar package on Debian-based systems).

### Amiga Side

For Amiga script to work, you'll need [LHA](http://aminet.net/package/util/arc/lha_68k) and [LZX](http://aminet.net/package/util/arc/lzx121r1) archivers for Amiga, and Workbench 3.0 or later (it probably works on 2.0 too, but for now it is not tested).

## 1.2.2 Update - Important!

Retroplay changed directory names for titles starting with a number from "#" to "0". If you use this script, please change the followng directory names
```
Retroplay/Commodore_Amiga_-_WHDLoad_-_Games/#/
Retroplay/Commodore_Amiga_-_WHDLoad_-_Demos/#/
```
to
```
Retroplay/Commodore_Amiga_-_WHDLoad_-_Games/0/
Retroplay/Commodore_Amiga_-_WHDLoad_-_Demos/0/
```
and download updated script.

## Thanks

Thanks to Retroplay for maintaining Amiga WHDLoad game archive.
Thanks fo Foul for bug report and testing of the script.

## History

#### 1.0.1

* Initial public release

#### 1.1

* Switched from 7z to The Unarchiver (unar package on Linux) for unpacking on PC side
* Added support for LZX archives (if same version of file are available both as LHA and LZX, LZX will be used)
* Added Debug mode of execution (doesn't change local files)
* Added current FTP list to local database
* Script now checks for archives that aren't on FTP server, but exist in local directories
* Backup of local database is made before writing a new one
* Many small improvements to code
* Cleaned and updated README.md
* New bugs?

#### 1.2

* Added more checks while analyzing archives and deleting files
* Added support to erase archives and installations that are removed from Retroplay's FTP
* Changed variable names for AmigaDOS script generation

#### 1.2.1

* Added check for FTP connection
* Changed directory names for titles starting with number

#### 1.2.2

* Fixed check for FTP connection
* Added files counter when downloading
* Added progress bar when downloading (python-progressbar is now a required package)
* Fixed paths inside Directories.tar.gz
* More checks for filenames and unicode fixes
* Fixed a typo

#### 1.2.5

* Added logging, all output is now written to PyRetroplay.log
* Program now uses 7zip, the unarchiver and lhasa for unpacking and listing
* Added check for 7zip, the unarchiver and lhasa packages
* More unicode fixes

#### 1.2.6

* More unicode fixes
* More error checking while handling filenames

